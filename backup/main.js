var root = document.getElementById('root')
var controls = document.getElementById('controls')

navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

var AudioContext = window.AudioContext || window.webkitAudioContext
var audioCtx = new AudioContext()

function extend(out) {
  out = out || {};

  for (var i = 1; i < arguments.length; i++) {
    if (!arguments[i])
      continue;

    for (var key in arguments[i]) {
      if (arguments[i].hasOwnProperty(key))
        out[key] = arguments[i][key];
    }
  }

  return out;
};

var SliderControl = function(props) {
  var that = this
  var defaultProps = {
    parent: document.body,
    value: 0,
    min: 0,
    max: 1,
  }
  var rootClassName = 'Slider'

  this.props = extend({}, defaultProps, props);

  this._wrapper = document.createElement('div');
  this._track = document.createElement('div');
  this._progress = document.createElement('div');
  this._handler = document.createElement('div');

  this._wrapper.className = rootClassName;
  this._track.className = rootClassName + '_track';
  this._progress.className = rootClassName + '_progress';
  this._handler.className = rootClassName + '_handler';

  this._value = this.props.value;
  this._width = 0;
  this._left = 0;

  this._wrapper.onmousedown = function (e) {
    startDrag(e)
  }

  var checkDefaultValue = function () {
    var props = that.props;
    var value = props.value;
    var min = props.min;
    var max = props.max;

    if (value > max) {
      that._value = max
    } else if (value < min) {
      that._value = min
    }
  }

  var setDefaultPosition = function () {
    checkDefaultValue()
    var props = that.props
    var percents = (that._value - props.min) / (props.max - props.min) * 100
    that._progress.style.width = percents + '%';
  }

  var renderCaptions = function() {
    var captionClassName = rootClassName + '_caption';

    var left = document.createElement('div');
    left.className = captionClassName + ' ' + captionClassName + '--min';
    var leftText = that.props.minCaption || that.props.min;
    left.innerHTML = leftText
    that._wrapper.appendChild(left)

    var right = document.createElement('div');
    right.className = captionClassName + ' ' + captionClassName + '--max';
    var rightText = that.props.maxCaption || that.props.max;
    right.innerHTML = rightText
    that._wrapper.appendChild(right)
  }

  var renderSlider = function () {
    that._wrapper.appendChild(that._track);
    that._progress.appendChild(that._handler);
    that._track.appendChild(that._progress);
    setDefaultPosition()
    renderCaptions()
    that.props.parent.appendChild(that._wrapper);
  }

  var initSizes = function() {
    that._width = that._wrapper.clientWidth
    var coords = that._wrapper.getBoundingClientRect()
    that._left = coords.x
  }

  var setProgress = function(e) {
    var pageX = e.pageX
    var partValue = (pageX - that._left) / that._width
    var props = that.props
    var min = props.min
    var max = props.max

    if (partValue >= 1) {
      if (that._value === max) {
        return
      }
      partValue = 1
    } else if (partValue <= 0) {
      if (that._value === min) {
        return
      }
      partValue = 0
    }

    var value = min + (max - min) * partValue
    that._value = value
    that._progress.style.width = partValue * 100 + '%'

    if (typeof props.onChange === 'function') {
      props.onChange(that._value)
    }
  }

  var stopDrag = function() {
    that._handler.classList.remove('Slider_handler--active');
    window.removeEventListener('mouseup', stopDrag);
    window.removeEventListener('mousemove', setProgress);
  }

  var startDrag = function(e) {
    initSizes()
    setProgress(e)
    that._handler.classList.add('Slider_handler--active');
    window.addEventListener('mouseup', stopDrag);
    window.addEventListener('mousemove', setProgress);
  }

  renderSlider()
}


var PanNode = function (ctx) {
  var that = this
  this.ctx = ctx
  this.pan = this.ctx.createStereoPanner()
  var currentTime = this.ctx.currentTime

  var setPanValue = function (value) {
    that.pan.pan.setValueAtTime(value, currentTime, 0)
  }

  setPanValue(0)

  this.setChanel = function (value) {
    setPanValue(value)
  }

  this.getChanel = function () {
    return this.pan.pan.value;
  }

}

var GainNode = function (ctx) {
  var that = this
  var DEFAULT_VOLUME = 1
  this._inited = false
  this.ctx = ctx;
  this.volume = this.ctx.createGain()
  var currentTime = this.ctx.currentTime

  var setVolumeValue = function (value) {
    that.volume.gain.setValueAtTime(value, currentTime, 0)
    this._inited = true
  }

  setVolumeValue(DEFAULT_VOLUME)

  this.setVolume = function (value) {
    setVolumeValue(value)
  }

  this.getVolume = function () {
    return this._inited ? this.volume.gain.value : DEFAULT_VOLUME;
  }
}

var Sound = function (ctx) {
  var that = this
  this.ctx = ctx;
  var currentTime = this.ctx.currentTime
  this.oscillator = this.ctx.createOscillator();
  this.gainNode = this.ctx.createGain()

  this.oscillator.connect(this.gainNode)

  this.node = this.gainNode

  this.play = function (value) {
    this.oscillator.frequency.setValueAtTime(value, currentTime, 0)
    this.gainNode.gain.setValueAtTime(1, currentTime, 0)
    this.oscillator.start(currentTime);
    this.stop()
  }

  this.stop = function() {
    this.gainNode.gain.exponentialRampToValueAtTime(0.001, currentTime + 1);
    this.oscillator.stop(currentTime + 1);
  }
}

var Curve = function () {
  var that = this
  var canva = document.createElement('canvas')
  var ctx = canva.getContext('2d')
  this.height = 0
  this.width = 0

  var setCanvasSizes = function () {
    that.height = window.innerHeight
    that.width = window.innerWidth
    canva.width = that.width
    canva.height = that.height
  }

  var onResize = function () {
    setCanvasSizes()
    that.draw()
  }

  window.addEventListener('resize', onResize)
  canva.className = 'CurveCanvas'
  setCanvasSizes()
  document.body.appendChild(canva)


  this.draw = function () {
    ctx.fillStyle = '#666';
    ctx.strokeStyle = '#666';
    ctx.beginPath();
    ctx.moveTo(0, this.height / 2)
    ctx.lineTo(30, this.height / 2 + 40)
    ctx.lineTo(300, this.height / 2 + 100)
    ctx.stroke()
  }
  this.draw()
}

var curve = new Curve()

var destination = audioCtx.destination
var panNode = new PanNode(audioCtx)
var gainNode = new GainNode(audioCtx)

gainNode.volume.connect(panNode.pan)
panNode.pan.connect(destination)

var handleSliderChange = function (value) {
  panNode.setChanel(value)
}

var slider = new SliderControl({
  onChange: handleSliderChange,
  parent: controls,
  value: panNode.getChanel(),
  min: -1,
  max: 1,
  minCaption: 'L',
  maxCaption: 'R',
})

var handleVolumeChange = function (value) {
  gainNode.setVolume(value)
}

var volumeSlider = new SliderControl({
  onChange: handleVolumeChange,
  parent: controls,
  value: gainNode.getVolume(),
  min: 0,
  max: 1,
  maxCaption: '100%',
})

var playButton = document.getElementById('play-button')

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


playButton.onclick = function () {
  var value = getRandomInt(40, 440)
  var sound = new Sound(audioCtx)
  sound.node.connect(gainNode.volume)
  sound.play(value)
}