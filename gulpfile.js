
'use-strict'

const gulp = require('gulp');
const paths = require('./config/paths');

function lazyRequireTasks(taskName, path, options) {
  options = options || {};
  options.taskName = taskName;
  gulp.task(taskName, function (callback) {
    let task = require(path).call(this, options);

    return task(callback)
  })
};

//  Tasks
lazyRequireTasks('build:style', './tasks/buildStyles', {
  src: paths.src.style,
  dest: paths.build.css,
});

lazyRequireTasks('build:js', './tasks/buildJs', {
  src: paths.src.js,
  dest: paths.build.js,
});

lazyRequireTasks('build:html', './tasks/buildHtml', {
  src: paths.src.html,
  dest: paths.build.html,
});

lazyRequireTasks('build:img', './tasks/buildImg', {
  src: paths.src.img,
  dest: paths.build.img,
});

lazyRequireTasks('serve', './tasks/serve', {
  dest: paths.build.root,
});

lazyRequireTasks('clean', './tasks/clean', {
  dest: paths.build.root,
});

gulp.task('build', gulp.series(
  'clean',
  gulp.parallel('build:style', 'build:html', 'build:js', 'build:img'))
);

gulp.task('watch', function () {
  gulp.watch(paths.watch.js, gulp.series('build:js'));
  gulp.watch(paths.watch.style, gulp.series('build:style'));
  gulp.watch(paths.watch.html, gulp.series('build:html'));
  gulp.watch(paths.watch.img, gulp.series('build:img'));
});

gulp.task('start', gulp.series('build', gulp.parallel('watch', 'serve')));