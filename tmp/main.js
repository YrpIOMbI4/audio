var ANALYSER_FFT_SIZE = 256
var ANALYSER_SMOOTHING_TIME_CONSTANT = 0
var DEFAULT_VOLUME = 1

var NOTES = [
  {
    name: 'C',
    freq: 261.6,
  },
  {
    name: 'C#',
    freq: 277.2,
  },
  {
    name: 'D',
    freq: 293.7,
  },
  {
    name: 'Eb',
    freq: 311.1,
  },
  {
    name: 'E',
    freq: 329.6,
  },
  {
    name: 'F',
    freq: 349.26,
  },
  {
    name: 'F#',
    freq: 370.0,
  },
  {
    name: 'G',
    freq: 392.0,
  },
  {
    name: 'G#',
    freq: 415.3,
  },
  {
    name: 'A',
    freq: 440.0,
  },
  {
    name: 'Bb',
    freq: 466.20,
  },
  {
    name: 'B',
    freq: 493.9,
  },
]

navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

var AudioContext = window.AudioContext || window.webkitAudioContext

function extend(out) {
  out = out || {};

  for (var i = 1; i < arguments.length; i++) {
    if (!arguments[i])
      continue;

    for (var key in arguments[i]) {
      if (arguments[i].hasOwnProperty(key))
        out[key] = arguments[i][key];
    }
  }

  return out;
};
var Sound = function (ctx) {
  var that = this

  this.ctx = ctx;
  var currentTime = this.ctx.currentTime
  this.oscillator = this.ctx.createOscillator();
  this.gainNode = this.ctx.createGain()

  this.oscillator.connect(this.gainNode)
  this.oscillator.type = 'sawtooth'
  this.node = this.gainNode

  this.play = function (value) {
    this.oscillator.frequency.setValueAtTime(value, currentTime, 0)
    this.gainNode.gain.setValueAtTime(1, currentTime, 0)
    this.oscillator.start(currentTime);
    this.stop()
  }

  this.stop = function() {
    this.gainNode.gain.exponentialRampToValueAtTime(0.001, currentTime + 1);
    this.oscillator.stop(currentTime + 1);
  }
}
var GainNode = function (ctx) {
  var that = this
  this._inited = false
  this.ctx = ctx;
  this.volume = this.ctx.createGain()
  var currentTime = this.ctx.currentTime

  var setVolumeValue = function (value) {
    that.volume.gain.setValueAtTime(value, currentTime, 0)
    this._inited = true
  }

  setVolumeValue(DEFAULT_VOLUME)

  this.setVolume = function (value) {
    setVolumeValue(value)
  }

  this.getVolume = function () {
    return this._inited ? this.volume.gain.value : DEFAULT_VOLUME;
  }
}
var Analyser = function (ctx) {
  var that = this
  this.ctx = ctx;
  this.analyser = this.ctx.createAnalyser()
  this.node = this.ctx.createScriptProcessor(2048, 1, 1);
  this.analyser.fftSize = ANALYSER_FFT_SIZE
  this.analyser.smoothingTimeConstant = ANALYSER_SMOOTHING_TIME_CONSTANT;

  this.fFrequencyData = new Float32Array(this.analyser.frequencyBinCount);
  this.bFrequencyData = new Uint8Array(this.analyser.frequencyBinCount);
  this.bTimeData = new Uint8Array(this.analyser.frequencyBinCount);

  this.analyser.connect(this.node)

  this.node.onaudioprocess = function () {
    that.analyser.getFloatFrequencyData(that.fFrequencyData);
    that.analyser.getByteFrequencyData(that.bFrequencyData);
    that.analyser.getByteTimeDomainData(that.bTimeData);
      return typeof that.update === "function" ? that.update(that.bFrequencyData) : 0;
  };

  return this
}
var PanNode = function (ctx) {
  var that = this
  this.ctx = ctx
  this.pan = this.ctx.createStereoPanner()
  var currentTime = this.ctx.currentTime

  var setPanValue = function (value) {
    that.pan.pan.setValueAtTime(value, currentTime, 0)
  }

  setPanValue(0)

  this.setChanel = function (value) {
    setPanValue(value)
  }

  this.getChanel = function () {
    return this.pan.pan.value;
  }
}

var MusicButton = function (note) {
  var _that = this;
  this.note = note;
  this.button = document.createElement('div');
  this.button.className = 'Button';
  this.button.innerHTML = this.note.name;
  this.button.onmousedown = function () {
    playSound(_that.note.freq);
  }

  return this.button;
}
var SliderControl = function(props) {
  var that = this
  var defaultProps = {
    parent: document.body,
    value: 0,
    min: 0,
    max: 1,
  }
  var rootClassName = 'Slider'

  this.props = extend({}, defaultProps, props);

  this._wrapper = document.createElement('div');
  this._track = document.createElement('div');
  this._progress = document.createElement('div');
  this._handler = document.createElement('div');

  this._wrapper.className = rootClassName;
  this._track.className = rootClassName + '_track';
  this._progress.className = rootClassName + '_progress';
  this._handler.className = rootClassName + '_handler';

  this._value = this.props.value;
  this._width = 0;
  this._left = 0;

  this._wrapper.onmousedown = function (e) {
    startDrag(e)
  }

  var checkDefaultValue = function () {
    var props = that.props;
    var value = props.value;
    var min = props.min;
    var max = props.max;

    if (value > max) {
      that._value = max
    } else if (value < min) {
      that._value = min
    }
  }

  var setDefaultPosition = function () {
    checkDefaultValue()
    var props = that.props
    var percents = (that._value - props.min) / (props.max - props.min) * 100
    that._progress.style.width = percents + '%';
  }

  var renderCaptions = function() {
    var captionClassName = rootClassName + '_caption';

    var left = document.createElement('div');
    left.className = captionClassName + ' ' + captionClassName + '--min';
    var leftText = that.props.minCaption || that.props.min;
    left.innerHTML = leftText
    that._wrapper.appendChild(left)

    var right = document.createElement('div');
    right.className = captionClassName + ' ' + captionClassName + '--max';
    var rightText = that.props.maxCaption || that.props.max;
    right.innerHTML = rightText
    that._wrapper.appendChild(right)
  }

  var renderSlider = function () {
    that._wrapper.appendChild(that._track);
    that._progress.appendChild(that._handler);
    that._track.appendChild(that._progress);
    setDefaultPosition()
    renderCaptions()
    that.props.parent.appendChild(that._wrapper);
  }

  var initSizes = function() {
    that._width = that._wrapper.clientWidth
    var coords = that._wrapper.getBoundingClientRect()
    that._left = coords.x
  }

  var setProgress = function(e) {
    var pageX = e.pageX
    var partValue = (pageX - that._left) / that._width
    var props = that.props
    var min = props.min
    var max = props.max

    if (partValue >= 1) {
      if (that._value === max) {
        return
      }
      partValue = 1
    } else if (partValue <= 0) {
      if (that._value === min) {
        return
      }
      partValue = 0
    }

    var value = min + (max - min) * partValue
    that._value = value
    that._progress.style.width = partValue * 100 + '%'

    if (typeof that.onChange === 'function') {
      that.onChange(that._value)
    }
  }

  var stopDrag = function() {
    that._handler.classList.remove('Slider_handler--active');
    window.removeEventListener('mouseup', stopDrag);
    window.removeEventListener('mousemove', setProgress);
  }

  var startDrag = function(e) {
    initSizes()
    setProgress(e)
    that._handler.classList.add('Slider_handler--active');
    window.addEventListener('mouseup', stopDrag);
    window.addEventListener('mousemove', setProgress);
  }

  renderSlider()
}

var SVGCurve = function () {
  var _that = this
  var svgns = "http://www.w3.org/2000/svg";

  this.width = window.innerWidth;
  this.height = window.innerHeight;
  this.svg = document.createElementNS(svgns, 'svg');
  this.svg.setAttribute('class', 'SVGCurve');
  this.svg.setAttribute('viewBox', '0 0 ' + this.width + ' ' + this.height);
  this.svg.setAttribute('version', '1.1');
  this.svg.setAttribute('xmnls', svgns);
  this.svg.setAttribute('height', this.height);
  this.svg.setAttribute('width', this.width);

  this.path = document.createElementNS(svgns, 'path')
  this.pathAttr = ''
  this.path.setAttribute('class', 'SVGCurve_path')
  this.path.setAttribute('d', this.pathAttr)

  this.svg.appendChild(this.path)
  document.body.appendChild(this.svg)
}

SVGCurve.prototype = {
  generatePath: function (bands) {
    var len = bands.length
    var step = this.width / (len - 1)
    var x = 0;
    var y = this.height;
    this.pathAttr = 'M ' + x + ', ' + y;
    for (var i = 0; i < len; i++) {
      x = x + step
      y = this.height - bands[i]
      this.pathAttr = this.pathAttr + 'L ' + x + ', ' + y + ' '
    }
    this.pathAttr = this.pathAttr + 'L ' + this.width + ', ' + this.height
    this.path.setAttribute('d', this.pathAttr)
  }
}



var root = document.getElementById('root')
var controls = document.getElementById('controls')

var audioCtx = new AudioContext()
var svg = new SVGCurve()

var destination = audioCtx.destination
var panNode = new PanNode(audioCtx)
var gainNode = new GainNode(audioCtx)
var analyser = new Analyser(audioCtx)

gainNode.volume.connect(panNode.pan)

panNode.pan.connect(analyser.analyser)
analyser.analyser.connect(destination)

panNode.pan.connect(analyser.node)
analyser.node.connect(destination)

analyser.update = function (bands) {
  svg.generatePath(bands)
}

var chanelsSlider = new SliderControl({
  parent: controls,
  value: panNode.getChanel(),
  min: -1,
  max: 1,
  minCaption: 'L',
  maxCaption: 'R',
})

var volumeSlider = new SliderControl({
  parent: controls,
  value: gainNode.getVolume(),
  min: 0,
  max: 1,
  maxCaption: '100%',
})

chanelsSlider.onChange = function (value) {
  panNode.setChanel(value)
}

volumeSlider.onChange = function (value) {
  gainNode.setVolume(value)
}

var playButton = document.getElementById('play-button')
var playButton2 = document.getElementById('play-button-1')

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

var playSound = function (freq) {
  var sound = new Sound(audioCtx)
  sound.node.connect(gainNode.volume)
  sound.play(freq)
}

var notesLength = NOTES.length

var buttonsGroup = document.getElementById('buttons-group');
for (var i = 0; i < notesLength; i++) {
  var btn = new MusicButton(NOTES[i])
  buttonsGroup.appendChild(btn)
}