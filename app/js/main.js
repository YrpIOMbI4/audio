
import { getMaxOfArray } from './parts/helpers'

import Analyser from './parts/analyser';
import AudioTrack from './parts/audioTrack/audioTrack';
import PanNode from './parts/pan';
import GainNode from './parts/gain';
import SliderControl from './parts/sliderControl';
import SVGCurve from './parts/svgCurve';
import MusicButton from './parts/musicButton';
import Piano from './parts/piano'
import Consts from './parts/constants'

import PieChart from './parts/pieChart'

import Notes from './parts/notes';

import Recorder from './parts/recorder'
import RecordButton from './parts/recordButton'


const AudioContext = window.AudioContext || window.webkitAudioContext
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

const root = document.getElementById('root')
const controls = document.getElementById('controls')


const audioCtx = new AudioContext()

const destination = audioCtx.destination
const panNode = new PanNode(audioCtx)
const gainNode = new GainNode(audioCtx)
const analyser = new Analyser(audioCtx)

gainNode.connect(panNode.node)
panNode.connect(analyser.analyser)
analyser.analyser.connect(destination)

panNode.pan.connect(analyser.node)
analyser.node.connect(destination)


//  SVG Curve
const svg = new SVGCurve({
  bezier: true,
  className: 'analyzerCurve',
  startFrom: Consts.CURVE_START_FROM.BOTTOM
})
svg.render()

analyser.update = function (bands) {
  svg.generatePath(bands)
}

var chanelsSlider = new SliderControl({
  parent: controls,
  value: panNode.getChanel(),
  min: -1,
  max: 1,
  minCaption: 'L',
  maxCaption: 'R',
})

var volumeSlider = new SliderControl({
  parent: controls,
  value: gainNode.getVolume(),
  min: 0,
  max: 1,
  maxCaption: '100%',
})

chanelsSlider.onChange = function (value) {
  panNode.setChanel(value)
}

volumeSlider.onChange = function (value) {
  gainNode.setVolume(value)
}

const buttonsGroup = document.getElementById('buttons-group');

const switchAnalyser = (analyze) => {
  analyser.analyze = analyze
}

const piano = new Piano({
  ctx: audioCtx,
  dest: gainNode.volume,
  onPlay: switchAnalyser
})

piano.renderTo(buttonsGroup)

//  TEST

class Graph {
  constructor(props) {
    this._rootClassName = 'graph'
    this._props = props
    this._curve = new SVGCurve({
      className: `${this._rootClassName}_curve`,
      bezier: true,
    })
    this._controls = {}
    this._data = []
    this.initData()
    this._controlsWrapper = document.createElement('div')
    this._controlsWrapper.className = `${this._rootClassName}_controls`
  }

  initData = () => {
    for (let i = 0; i < this._props.count; i++) {
      this._data[i] = 0
    }
  }

  render = (parent) => {
    const root = document.createElement('div')
    root.className = this._rootClassName

    const curveWrapper = document.createElement('div')
    root.className = `${this._rootClassName}_curveWrapper`

    this._curve.render(curveWrapper)

    root.appendChild(curveWrapper)

    this.drawControls()
    root.appendChild(this._controlsWrapper)

    parent.appendChild(root)
    this._curve.generatePath(this._data)
  }

  drawControls = () => {
    for (let i = 0; i < this._props.count; i++) {
      const control = new SliderControl({
        vertical: true,
        parent: this._controlsWrapper,
        min: -1,
        max: 1,
        value: 0,
        showCaptions: false,
        className: `${this._rootClassName}_control`
      })
      control.onChange = (val) => {
        this._data[i] = val * 400
        this._curve.generatePath(this._data)
      }
    }
  }

}

const graph = new Graph({ count: 10 })
const testNode = document.getElementById('test')
graph.render(testNode)


// Recorder init

const recorder = new Recorder({
  audioCtx: audioCtx,
  dest: destination,
})

const recordButton = new RecordButton()
const recorderWrapper = document.getElementById('recorder-wrapper')
recordButton.renderTo(recorderWrapper)

recorder.onRecording = (buffer) => {
  const data = getMaxOfArray(buffer)
  console.log(buffer)
  recordButton.onRecording(data)
}

recorder.onRecorded = function (url, buffers) {
  const track = new AudioTrack(url, buffers)
  track.render(recorderWrapper)
}

recordButton.onClick = function () {
  recorder.toggle()
  recordButton.setRecordClass(recorder.isRecording)
}

const SECTORS = [
  {
    id: 'first',
    number: 50,
    name: 'Кусок',
  },
  {
    id: 'second',
    number: 640,
    name: 'Ебужиный кусок',
  },
  {
    id: 'third',
    number: 30,
    name: 'Фигня',
  },
  {
    id: 'third',
    number: 230,
    name: 'Ну так, норм',
  },
  {
    id: 'third',
    number: 30,
    name: 'Хуета',
  },
  {
    id: 'third',
    number: 260,
    name: 'Заебись',
  },
  {
    id: 'third',
    number: 100,
    name: 'Ни туда ни сюда',
  },
  {
    id: 'third',
    number: 100,
    name: 'Пирога кусок',
  },
]

const pieChart = new PieChart({
  data: SECTORS,
  diameter: 200,
  innerDiameter: 50,
})

pieChart.renderTo(root)
