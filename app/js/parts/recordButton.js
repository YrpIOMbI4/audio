import { createNode } from './helpers'

class RecordButton {
  constructor() {
    this._rootClassName = 'RecButton'
    this._root = createNode('div', this._rootClassName)

    this._root.onclick = this._handleClick
  }

  _handleClick = (e) => {
    if (typeof this.onClick === 'function') {
      this.onClick()
    }
  }

  setRecordClass = (active) => {
    const recordingClassName = `${this._rootClassName}--isRecording`
    if (active) {
      this._root.classList.add(recordingClassName)
    } else {
      this._root.classList.remove(recordingClassName)
    }
  }

  onRecording = (data) => {
    // const int = setInterval(() => {
    //   this._root.style.boxShadow = `0 0 ${data * 100}px #000, 0 0 ${data * 100}px #000, 0 0 ${data * 100}px #000, 0 0 ${data * 100}px #000`
    // }, 100)
  }

  renderTo = (parent) => {
    parent.appendChild(this._root)
  }
}

export default RecordButton
