import { createNode } from './helpers'

class SliderControl {
  constructor(props) {
    const defaultProps = {
      parent: document.body,
      value: 0,
      min: 0,
      max: 1,
      vertical: false,
      showCaptions: true,
      highlightProgress: true,
    }
    this._rootClassName = 'Slider'
    this.props = { ...defaultProps, ...props };

    this._wrapper = document.createElement('div');
    this._track = this._renderTrack()
    this._progress = document.createElement('div');

    this._handlerClassName = ''
    this._trackClassName = ''
    this._handler = this._renderHandler()

    this._wrapper.className = this._rootClassName
    if (this.props.vertical) {
      this._wrapper.classList.add(`${this._rootClassName}--vertical`)
    }
    if (this.props.className) {
      this._wrapper.className += ` ${this.props.className}`
    }
    this._progress.className = `${this._rootClassName}_progress`;

    if (this.props.highlightProgress) {
      this._progress.classList.add(`${this._progress.className}--highlight`)
    }

    this._value = this.props.value;
    this._size = 0;
    this._start = 0;

    this._wrapper.onmousedown = (e) => {
      this.startDrag(e)
    }

    this.render(this.props.parent)
  }

  get element() {
    return this._wrapper
  }

  _renderTrack() {

    const node = document.createElement('div');
    node.className = `${this._rootClassName}_track`

    if (!this.props.trackNode) {
      node.classList.add(`${this._rootClassName}_trackNative`);
    } else {
      node.appendChild(this.props.trackNode)
    }

    return node
  }

  _renderHandler() {
    let node
    let className
    if (!!this.props.handlerNode) {
      node = this.props.handlerNode
      className = node.className
    } else {
      node = document.createElement('div');
      className = `${this._rootClassName}_handlerNative`
      node.className = className
    }

    node.classList.add(`${this._rootClassName}_handler`);
    this._handlerClassName = className
    return node
  }

  checkDefaultValue = (newValue) => {
    const props = this.props;
    const value = newValue || props.value;
    const min = props.min;
    const max = props.max;

    if (value > max) {
      this._value = max
    } else if (value < min) {
      this._value = min
    }
  }

  setDefaultPosition = (value) => {
    this.checkDefaultValue()
    const props = this.props
    const percents = (this._value - props.min) / (props.max - props.min) * 100
    const stylePerc = `${percents}%`
    if (this.props.vertical) {
      this._progress.style.height = stylePerc;
    } else {
      this._progress.style.width = stylePerc;
    }
  }

  renderCaptions = () => {
    const captionClassName = this._rootClassName + '_caption';

    const left = document.createElement('div');
    left.className = captionClassName + ' ' + captionClassName + '--min';
    const leftText = this.props.minCaption || this.props.min;
    left.innerHTML = leftText
    this._wrapper.appendChild(left)

    const right = document.createElement('div');
    right.className = captionClassName + ' ' + captionClassName + '--max';
    const rightText = this.props.maxCaption || this.props.max;
    right.innerHTML = rightText
    this._wrapper.appendChild(right)
  }

  render = (parent) => {
    this._wrapper.appendChild(this._track);
    this._progress.appendChild(this._handler);
    this._track.appendChild(this._progress);
    this.setDefaultPosition()
    if (this.props.showCaptions) {
      this.renderCaptions()
    }
    const parentNode = parent || this.props.parent
    if (!parentNode) {
      return
    }
    parentNode.appendChild(this._wrapper);
  }

  initSizes = () => {
    const coords = this._wrapper.getBoundingClientRect()
    this._size = this.props.vertical ? this._wrapper.clientHeight : this._wrapper.clientWidth
    this._start = this.props.vertical ? coords.y + this._size : coords.x
  }

  setProgress = (e) => {
    const mousePosition = this.props.vertical ? e.clientY : e.clientX
    let partValue = (mousePosition - this._start) / this._size
    if (this.props.vertical) {
      partValue = partValue * (-1)
    }
    const props = this.props
    const min = props.min
    const max = props.max

    if (partValue >= 1) {
      if (this._value === max) {
        return
      }
      partValue = 1
    } else if (partValue <= 0) {
      if (this._value === min) {
        return
      }
      partValue = 0
    }

    const value = min + (max - min) * partValue
    this._value = value
    const styleProgress = `${partValue * 100}%`
    if (this.props.vertical) {
      this._progress.style.height = styleProgress
    } else {
      this._progress.style.width = partValue * 100 + '%'
    }

    if (typeof this.onChange === 'function') {
      this.onChange(this._value)
    }
  }

  setValue = (value) => {
    this._value = value
    this.setDefaultPosition(this._value)
  }

  stopDrag = () => {
    if (typeof this.onStopDrag === 'function') {
      this.onStopDrag(this._value)
    }
    this._handler.classList.remove(`${this._handlerClassName}--active`);
    window.removeEventListener('mouseup', this.stopDrag);
    window.removeEventListener('mousemove', this.setProgress);
  }

  startDrag = (e) => {
    this.initSizes()
    this.setProgress(e)
    this._handler.classList.add(`${this._handlerClassName}--active`);
    window.addEventListener('mouseup', this.stopDrag);
    window.addEventListener('mousemove', this.setProgress);
  }
}

export default SliderControl
