const COEFFICIENT = 1.0594630943592953

class Notes {
  constructor(octaveNum) {
    this._octaveNum = octaveNum
    this._noteReference = {
      name: 'A',
      freq: 440.0,
      number: 10,
      octave: 1,
    }

    this._notesNames = ['C', 'C#', 'D', 'Eb', 'E', 'F', 'F#', 'G', 'G#', 'A', 'Bb', 'B']
    return this.calculate(this._octaveNum)
  }

  generateNote(name, freq, number, octave) {
    return {
      name,
      freq,
      number,
      octave
    }
  }

  calculate = (octaveNum) => {
    const notesLength = this._notesNames.length
    const refFreq = this._noteReference.freq
    const refNumber = this._noteReference.number
    const octave = []

    for (let i = 0; i < notesLength; i ++) {
      let freq
      let number = i + 1
      if (number < refNumber) {
        freq = refFreq / Math.pow(COEFFICIENT, refNumber - number)
      } else if (number === refNumber) {
        freq = refFreq
      } else {
        freq = refFreq * Math.pow(COEFFICIENT,(number - refNumber))
      }
      freq = freq / octaveNum
      octave.push(this.generateNote(this._notesNames[i], freq, number, octaveNum))
    }
    return octave
  }
}

export default Notes