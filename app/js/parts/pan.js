class PanNode {
  constructor(ctx) {
    this.ctx = ctx
    this.pan = this.ctx.createStereoPanner()
    this.currentTime = this.ctx.currentTime
    this.setPanValue(0)
  }

  setPanValue = (value) => {
    this.pan.pan.setValueAtTime(value, this.currentTime, 0)
  }

  setChanel = (value) => {
    this.setPanValue(value)
  }

  getChanel = () => {
    return this.pan.pan.value;
  }

  connect = (dest) => {
    this.pan.connect(dest)
  }

  get node() {
    return this.pan
  }
}

export default PanNode
