import { ANALYSER_SMOOTHING_TIME_CONSTANT, ANALYSER_FFT_SIZE } from './constants'

class Analyser {
  constructor(ctx) {
    this._analyze = false
    this.ctx = ctx;
    this.analyser = this.ctx.createAnalyser()
    this.node = this.ctx.createScriptProcessor(2048, 1, 1);
    this.analyser.fftSize = ANALYSER_FFT_SIZE
    this.analyser.smoothingTimeConstant = ANALYSER_SMOOTHING_TIME_CONSTANT;
    this.fFrequencyData = new Float32Array(this.analyser.frequencyBinCount);
    this.bFrequencyData = new Uint8Array(this.analyser.frequencyBinCount);
    this.bTimeData = new Uint8Array(this.analyser.frequencyBinCount);
    this.init();
  };

  set analyze(analyze) {
    this._analyze = analyze
  }

  checkData(data) {
    const dataLen = data.length
    for (let i = 0; i < dataLen; i++) {
      if (data[i] !== 0) {
        return
      } else if (i === dataLen -1 && data[i] === 0) {
        this._analyze = false
      }
    }
  }

  init = () => {
    this.node.onaudioprocess = () => {
      if (!this._analyze) {
        return
      }
      this.analyser.getFloatFrequencyData(this.fFrequencyData);
      this.analyser.getByteFrequencyData(this.bFrequencyData);
      this.analyser.getByteTimeDomainData(this.bTimeData);
      this.checkData(this.bFrequencyData)
      return typeof this.update === "function" ? this.update(this.bFrequencyData) : 0;
    };
  }


}

export default Analyser