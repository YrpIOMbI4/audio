class Recorder {
  constructor(props) {
    this._props = props
    this._audioCtx = this._props.audioCtx
    this._dest = this._props.dest
    this._isRecording = false
    this._mediaRecorder = {}
    this.init()
    this._chunk = []
    this._recBuffersL = []
    this._recBuffersR = []
    this._recLength = 0

    this._scriptProcessor = this._audioCtx.createScriptProcessor(256, 2, 2)
    this._scriptProcessor.onaudioprocess = this.onRecording

    this._inputPoint = this._audioCtx.createGain();
  }

  getMaxOfArray(numArray) {
    return Math.max.apply(null, numArray);
  }
  getMinOfArray(numArray) {
    return Math.min.apply(null, numArray);
  }

  onRecording = (e) => {
    if (!this._isRecording) {
      return
    }

    const inputBuffer = e.inputBuffer

    const lBuffer = inputBuffer.getChannelData(0)

    const rBuffer = inputBuffer.getChannelData(1)

    this._recBuffersL.push(...lBuffer);
    this._recBuffersR.push(...rBuffer);

    if (typeof this.onRecording === 'function') {
      this.onRecording(lBuffer)
    }
  }

  getBuffers = () => {
    return [this._recBuffersL, this._recBuffersR]
  }

  clear = () => {
    this._recBuffersL = [];
    this._recBuffersR = [];
    this._chunk = [];
  }

  onSuccess = (stream) => {

    this._input = this._audioCtx.createMediaStreamSource(stream)
    this._input.connect(this._inputPoint)
    this._inputPoint.connect(this._scriptProcessor)
    this._scriptProcessor.connect(this._audioCtx.destination)

    this._mediaRecorder = new MediaRecorder(stream)
    this._mediaRecorder.ondataavailable = this.write
    this._mediaRecorder.onstop = this.createAudio;
  }

  write = (e) => {
    this._chunk.push(e.data)
  }

  onError = (error) => {
  }

  init = () => {
    navigator.mediaDevices.getUserMedia({ audio: true })
      .then(this.onSuccess)
      .catch(this.onError)
  }

  record = () => {
    this._isRecording = true
    this._mediaRecorder.start()
  }

  createAudio = () => {
    const blob = new Blob(this._chunk, { 'type' : 'audio/mp3; codecs=opus' });
    const audioURL = window.URL.createObjectURL(blob);
    const buffers = this.getBuffers()
    if (typeof this.onRecorded === 'function') {
      this.onRecorded(audioURL, buffers)
    }
    this.clear()
  }

  stop = () => {
    this._isRecording = false;
    this._mediaRecorder.stop();
  }

  toggle = () => {
    if (this._isRecording) {
      this.stop()
    } else {
      this.record()
    }
  }

  get isRecording() {
    return this._isRecording;
  }

}

export default Recorder
