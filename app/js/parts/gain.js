import { DEFAULT_VOLUME } from './constants';

class GainNode {
  constructor(ctx) {
    this._inited = false
    this.ctx = ctx;
    this.volume = this.ctx.createGain()
    this.currentTime = this.ctx.currentTime
    this.setVolumeValue(DEFAULT_VOLUME)
  }

  setVolumeValue = (value) => {
    this.volume.gain.setValueAtTime(value, this.currentTime, 0)
    this._inited = true
  }

  setVolume = (value) => {
    this.setVolumeValue(value)
  }

  getVolume = () => {
    return this._inited ? this.volume.gain.value : DEFAULT_VOLUME;
  }

  connect = (dest) => {
    this.volume.connect(dest)
  }
}

export default GainNode