import { SVGNS } from './constants'

export function degToRad(angle) {
  return (Math.PI * angle) / 180
}

export function setStylesOnElement(styles, element){
    Object.assign(element.style, styles);
}

export function createNode(el, className, ns, width, height) {
  let node
  if (!ns) {
    node = document.createElement(el)
    node.className = className
  } else {
    node = document.createElementNS(SVGNS, el)
    node.setAttribute('class', className)
    if (el === 'svg') {
      node.setAttribute('version', '1.1');
      node.setAttribute('xmnls', SVGNS);

      if (width && height) {
        node.setAttribute('viewBox', `0 0 ${width} ${height}`);
        node.setAttribute('height', height);
        node.setAttribute('width', width);
      }
    }
  }
  return node
}

export function getMaxOfArray(numArray) {
  return Math.max.apply(null, numArray);
}

export default { degToRad, setStylesOnElement, createNode, getMaxOfArray }

