const TYPES = {
  DIEZ: 'diez',
  BEMOL: 'bemol',
}
class MusicButton {
  constructor(props) {
    this.props = props
    this.note = props.note;
    this.button = document.createElement('div');
    this.button.id = this.props.id
    this.button.className = `PianoPutton PianoPutton--${this.getType()}`;
  }

  get freq() {
    return this.note.freq
  }

  setIsActive(isActive) {
    if (isActive) {
      this.button.classList.add('PianoPutton--active')
    } else {
      this.button.classList.remove('PianoPutton--active')
    }
  }

  getType = () => {
    let type = TYPES.BEMOL
    if (this.note.name.match(/[#b]/)) {
      type = TYPES.DIEZ
    }
    return type
  }

}

export default MusicButton