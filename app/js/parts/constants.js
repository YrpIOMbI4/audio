export const ANALYSER_FFT_SIZE = 64;
export const ANALYSER_SMOOTHING_TIME_CONSTANT = 0;
export const DEFAULT_VOLUME = 1;
export const CURVE_START_FROM = {
  TOP: 'top',
  MIDDLE: 'middle',
  BOTTOM: 'bottom',
}

export const SVGNS = "http://www.w3.org/2000/svg";

export const POSITION = {
  LEFT: 'left',
  RIGHT: 'right',
  TOP: 'top',
  BOTTOM: 'bottom',
  BOTTOM_RIGHT: 'bottomRight',
  BOTTOM_LEFT: 'bottomLeft',
  TOP_RIGHT: 'topRight',
  TOP_LEFT: 'topLeft',
}

export const CONTROL_SIZE = 18;

export default {
  ANALYSER_FFT_SIZE,
  ANALYSER_SMOOTHING_TIME_CONSTANT,
  DEFAULT_VOLUME,
  CURVE_START_FROM,
  SVGNS,
  POSITION,
  CONTROL_SIZE,
}
