import { CURVE_START_FROM, SVGNS } from './constants'

class SVGCurve {
  constructor(props) {
    this.props = props || {}
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this._baseY = this.getBaseY()

    if (this.props.showHeplers) {
      this.helpers = {}
    }

    this._isRendered = false
    this._isFirstCalc = true
  }

  getBaseY() {
    switch(this.props.startFrom) {
      case CURVE_START_FROM.TOP:
        return 0
      case CURVE_START_FROM.BOTTOM:
        return this.height
      case CURVE_START_FROM.MIDDLE:
      default:
        return this.height / 2
    }
  }

  render(parent) {
    const rootClassName = 'SVGCurve'
    const className = !!this.props.className ? `${rootClassName} ${this.props.className}` : rootClassName
    this.svg = document.createElementNS(SVGNS, 'svg');
    this.svg.setAttribute('class', className);
    this.svg.setAttribute('viewBox', `0 0 ${this.width} ${this.height}`);
    this.svg.setAttribute('version', '1.1');
    this.svg.setAttribute('xmnls', SVGNS);
    this.svg.setAttribute('height', this.height);
    this.svg.setAttribute('width', this.width);

    this.path = document.createElementNS(SVGNS, 'path')
    this.path.setAttribute('class', 'SVGCurve_path')

    this.svg.appendChild(this.path)

    const dest = parent || document.body
    dest.appendChild(this.svg)
    this._isRendered = true
  }

  createPathPoint(i, x, y, lastX, lastY, nextX, nextY, len) {
    if (this.props.bezier) {

      const dX = nextX - x
      const sqrDX = dX * dX
      const prevDY = y - lastY
      const nextDY = y - nextY
      const midDY = nextY - lastY

      let k = 0
      if (prevDY !== nextDY && midDY !== 0) {
        k = (Math.sqrt(((prevDY * prevDY) + sqrDX) * ((nextDY * nextDY) + sqrDX)) - sqrDX - prevDY * nextDY) / (dX * midDY)
      }

      const smoothDX = dX / 2 * Math.sqrt(1 / (1 + k * k))

      const smoothX1 = x + smoothDX
      const smoothX2 = x - smoothDX
      const smoothY1 = y + k * smoothDX
      const smoothY2 = y - k * smoothDX

      const smoothPoint1 = `${smoothX1}, ${smoothY1}`
      const smoothPoint2 = `${smoothX2}, ${smoothY2}`

      if (this.props.showHelpers) {
        this.updateHelpers(i, smoothPoint2, smoothPoint1, x, y, x, y)
      }

      if (i === 0 ) {
        return `L ${x}, ${y} C ${smoothPoint1}`
      } if (i === len) {
        return `${smoothPoint2} ${x}, ${y}`
      }
      return `${smoothPoint2} ${x}, ${y} ${smoothPoint1}`
    } else {
      return `L ${x}, ${y}`
    }
  }

  drawHelpers(i, x0, y0, x, y) {
    this.helpers[i] = document.createElementNS(SVGNS, 'path')
    this.helpers[i].setAttribute('class', 'SVGCurve_path SVGCurve_path--helper SVGCurve_path--helper1')
    this.helpers[i].setAttribute('d', `M ${x0}, ${y0} L ${x}, ${y0}`)
    this.helpers[`${i}2`] = document.createElementNS(SVGNS, 'path')
    this.helpers[`${i}2`].setAttribute('class', 'SVGCurve_path SVGCurve_path--helper SVGCurve_path--helper2')
    this.helpers[`${i}2`].setAttribute('d', `M ${x0}, ${y0} L ${x}, ${y0}`)
    this.svg.appendChild(this.helpers[i])
    this.svg.appendChild(this.helpers[`${i}2`])
  }

  updateHelpers(i, smoothPoint1, smoothPoint2, x, y, lastX, lastY) {
    this.helpers[i].setAttribute('d', `M ${smoothPoint1} L ${lastX}, ${lastY}`)
    this.helpers[`${i}2`].setAttribute('d', `M ${smoothPoint2} L ${x}, ${y}`)
  }

  generatePath = (bands) => {
    if (!this._isRendered) {
      return
    }

    const len = bands.length + 1
    const step = this.width / len
    let x = 0
    let y = this._baseY
    const firstPoint = `M ${x}, ${y}`
    const pathArr = [firstPoint];

    for (let i = 0; i <= len; i++) {
      const lastX = x
      const lastY = y
      if (this._isFirstCalc && this.props.showHelpers) {
        this.drawHelpers(i, x, y, lastX, lastY)
      }
      if (i !== 0 && i < len) {
        x = step * i
        y = this._baseY - bands[i - 1]
      } else if (i === len) {
        x = this.width
        y = this._baseY
      }

      const nextY = i < len - 1 ? this._baseY - bands[i] : this._baseY
      const nextX = x + step
      pathArr.push(this.createPathPoint(i, x, y, lastX, lastY, nextX, nextY, len))

    }
    this.path.setAttribute('d', pathArr.join(' '))
    this._isFirstCalc = false
  }
}

export default SVGCurve
