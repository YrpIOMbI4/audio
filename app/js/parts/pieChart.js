import { SVGNS, POSITION } from './constants'
import Title from './title'
import { degToRad } from './helpers'

const CIRCLE_DEGREES = 360
const CHART_OFFSET = 50

class PieChart {
  constructor(props) {
    this._props = props;
    this._rootClassName = 'PieChart';
    this._total =  this._calculateTotal()

    this._wrapper = document.createElement('div')
    this._wrapper.className = `${this._rootClassName}-wrapper`
    this._root = this._renderRoot();
    this._sectors = this._renderSectors()
  }

  _calculateTotal() {
    let total = 0
    const len = this._props.data.length
    for (let i = 0; i < len; i++) {
      total += this._props.data[i].number
    }
    return total
  }

  _renderRoot() {
    const root = document.createElementNS(SVGNS, 'svg');
    const size = this._props.diameter + CHART_OFFSET * 2;
    root.setAttribute('xmnls', SVGNS);
    root.setAttribute('class', this._rootClassName);
    root.setAttribute('viewBox', `${ - CHART_OFFSET}, ${ - CHART_OFFSET}, ${size}, ${size}`);
    root.setAttribute('width', size);
    root.setAttribute('height', size);

    return root;
  };

  _calculateCoords(angle, radius, innerRadius) {
    const radAngle = degToRad(angle)
    const coords = {
      x: radius + radius * Math.cos(radAngle),
      y: radius + radius * Math.sin(radAngle),
    }
    return !innerRadius ? coords : {
      ...coords,
      innerX: radius + innerRadius * Math.cos(radAngle),
      innerY: radius + innerRadius * Math.sin(radAngle),
    };
  };

  _renderSectors() {
    const radius = this._props.diameter / 2;
    const innerRadius = this._props.innerDiameter / 2 || 0

    let endAngle = 0

    let prevPoint = {
      x: 0,
      y: 0,
      innerX: 0,
      innerY: 0,
    }

    return this._props.data.map((sector, i) => {
      const percents = sector.number / this._total;
      const angle = CIRCLE_DEGREES * percents;
      const reverted = angle > 180;

      const startAngle = endAngle;
      endAngle = startAngle + angle;

      const lastPoint = this._calculateCoords(endAngle, radius, innerRadius);
      const firstPoint = (i !== 0) ? prevPoint : this._calculateCoords(startAngle, radius, innerRadius);

      prevPoint = lastPoint;

      return this._renderSector(radius, innerRadius, firstPoint, lastPoint, reverted, i, sector.name, percents);
    })
  };

  _setTitlePosition(isOnRight, isOnBottom) {
    if (isOnRight && isOnBottom) {
      return POSITION.BOTTOM_RIGHT
    } else if (isOnRight && !isOnBottom) {
      return POSITION.TOP_RIGHT
    } else if (!isOnRight && isOnBottom) {
      return POSITION.BOTTOM_LEFT
    } else {
      return POSITION.TOP_LEFT
    }
  }

  _renderSector = (radius, innerRadius, firstPoint, lastPoint, reverted, i, name, percents) => {
    const sectorNode = document.createElementNS(SVGNS, 'path');
    const className = `${this._rootClassName}-sector`;
    sectorNode.setAttribute('class', className);
    sectorNode.classList.add(`${className}--${i}`);
    sectorNode.setAttribute('title', name);

    const transformOriginX = radius;
    const transformOriginY = radius;

    sectorNode.setAttribute('style', `transform-origin: ${transformOriginX}px ${transformOriginY}px`);

    const path = this._calculatePaths(radius, innerRadius, firstPoint, lastPoint, reverted);

    const isOnRight = (lastPoint.x + firstPoint.x) / 2 > radius
    const isOnBottom = (lastPoint.y + firstPoint.y) / 2 > radius

    const titleNode = new Title({
      title: `${name} — ${Math.ceil(percents * 100)}%`,
      y: lastPoint.y + CHART_OFFSET,
      x: isOnRight ? lastPoint.x + CHART_OFFSET : radius + lastPoint.x + CHART_OFFSET,
      position: this._setTitlePosition(isOnRight, isOnBottom),
    })

    sectorNode.onmouseover = () => {
      titleNode.toggle(true)
    }

    sectorNode.onmouseout = () => {
      titleNode.toggle(false)
    }

    sectorNode.setAttribute('d', path);
    return {
      sector: sectorNode,
      title: titleNode,
    }
  }

  _calculatePaths = (radius, innerRadius, firstPoint, lastPoint, reverted) => {
    let pathStart = `M ${radius}, ${radius}`;
    const pathFirstPoint = `L ${firstPoint.x}, ${firstPoint.y}`
    const pathArc = `A ${radius}, ${radius} 0 ${reverted ? 1 : 0}, 1 ${lastPoint.x}, ${lastPoint.y}`

    let path = `${pathStart}, ${pathFirstPoint}, ${pathArc} z`;

    if (innerRadius) {
      pathStart = `M ${firstPoint.innerX} ${firstPoint.innerY}`
      const pathLastPoint = `L ${lastPoint.innerX}, ${lastPoint.innerY}`
      const pathLastArc = `A ${innerRadius}, ${innerRadius} 0 ${reverted ? 1 : 0}, 0 ${firstPoint.innerX} ${firstPoint.innerY}`

      path = `${pathStart}, ${pathFirstPoint}, ${pathArc}, ${pathLastPoint}, ${pathLastArc} z`;
    }
    return path;
  }

  renderTo(parent) {
    const len = this._sectors.length
    for (let i = 0; i < len; i++) {
      this._root.appendChild(this._sectors[i].sector)
      this._sectors[i].title.renderTo(this._wrapper)
    }
    this._wrapper.appendChild(this._root);
    parent.appendChild(this._wrapper);
  }
}

export default PieChart
