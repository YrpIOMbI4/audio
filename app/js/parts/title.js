import { POSITION } from './constants'

const TITLE_MARGIN = 20

class Title {
  constructor(props) {
    const defaultProps = {
      position: POSITION.TOP,
      x: 0,
      y: 0,
    }
    this._props = { ...defaultProps, ...props }
    this._visible = false
    this._rootClassName = 'Title'
    this._root = this._renderRoot()
    this._width = 0
    this._height = 0
    this._calculated = false
  }

  _renderRoot() {
    const root = document.createElement('div')
    root.className = `${this._rootClassName} ${this._rootClassName}--${this._props.position}`
    root.innerHTML = this._props.title

    return root
  }

  // _setLeft() {
  //   this._root.style.marginRight = `${TITLE_MARGIN}px`;
  //   this._root.style.right = `${this._width + this._props.x}px`;
  // }

  _setLeft() {
    this._root.style.marginRight = `${TITLE_MARGIN}px`;
    this._root.style.right = `${this._props.x}px`;;
  }

  _setRight() {
    this._root.style.marginLeft = `${TITLE_MARGIN}px`;
    this._root.style.left = `${this._props.x}px`;
  }

  _setPosition() {
    switch(this._props.position) {
      case POSITION.BOTTOM:
        this._root.style.bottom = `${this._props.y}px`;
        return;
      case POSITION.LEFT:
        this._setLeft()
        return;
      case POSITION.RIGHT:
        this._root.style.marginLeft = `${TITLE_MARGIN}px`;
        this._root.style.left = `${this._props.x}px`;
        return;
      case POSITION.BOTTOM_RIGHT:
        this._root.style.marginLeft = `${TITLE_MARGIN}px`;
        this._root.style.left = `${this._props.x}px`;
        this._root.style.bottom = `${this._props.y}px`;
        return;
      case POSITION.BOTTOM_LEFT:
        this._setLeft()
        this._root.style.bottom = `${this._props.y}px`;
        return;
      case POSITION.TOP_RIGHT:
        this._root.style.marginLeft = `${TITLE_MARGIN}px`;
        this._root.style.left = `${this._props.x}px`;
        this._root.style.top = `${this._props.y}px`;
        return;
      case POSITION.TOP_LEFT:
        this._setLeft()
        this._root.style.top = `${this._props.y}px`;
        return;
      case POSITION.TOP:
      default:
        this._root.style.top = `${this._props.y}px`;
        return;
    }
  }

  toggle = (visible) => {
    if (visible && !this._calculated) {

      this._calculated = true
    }
    this._visible = visible
    const visibleClassName = `${this._rootClassName}--visible`
    if (this._visible) {
      this._root.classList.add(visibleClassName)
    } else {
      this._root.classList.remove(visibleClassName)
    }
  }

  renderTo(parent) {
    parent.style.position = 'relative'
    parent.appendChild(this._root)
    setTimeout(() => {
      this._width = this._root.clientWidth
      this._height = this._root.clientHeight
      this._setPosition()
    })
  }
}

export default Title
