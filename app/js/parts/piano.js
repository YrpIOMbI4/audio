import MusicButton from './musicButton';
import Notes from './notes';
import Sound from './sound';

class Piano {

  constructor(props) {
    this._props = props
    this._sound = {}
    this._notes = [...new Notes(3), ...new Notes(2), ...new Notes(1)]
    this._lastId = ''
    this._startPlaying = false

    this._wrapper = document.createElement('div')
    this._wrapper.className = 'piano__wrapper'
    this._wrapper.onmousedown = this.handleMouseDown

    this.renderButtons()
  }

  play = (freq) => {
    this._sound = new Sound({
      ctx: this._props.ctx,
      type: 'custom',
    })
    this._props.onPlay(true)
    this._sound.connect(this._props.dest)
    this._sound.play(freq)
  }

  stop = () => {
    if (this._sound && typeof this._sound.stop === 'function') {
      this._sound.stop(true)
    }
  }

  renderTo = (parent) => {
    parent.appendChild(this._wrapper)
  }

  pullButton() {
    if (this._lastId) {
      const prevButton = this._buttons[this._lastId]
      prevButton && prevButton.setIsActive(false)
    }
  }

  pushButtons(e) {
    const id = e.target.id
    if (id === this._lastId) {
      return
    }
    this.pullButton()
    this._lastId = id
    const button = this._buttons[id]
    if (!button) {
      return
    }
    button.setIsActive(true)
    this.play(button.freq)
  }

  handleMouseDown = (e) => {
    this.pushButtons(e)
    window.addEventListener('mouseup', this.stopPlaying);
    window.addEventListener('mousemove', this.playButtons);
  }

  playButtons = (e) => {
    this.pushButtons(e)
  }

  stopPlaying = () => {
    this.pullButton()
    //this.stop()
    this._lastId = ''
    window.removeEventListener('mouseup', this.stopPlaying);
    window.removeEventListener('mousemove', this.playButtons);
  }

  renderButtons() {
    const notesLength = this._notes.length
    this._buttons = {}
    for (let i = 0; i < notesLength; i++) {
      const note = this._notes[i]
      const id = `${note.octave}-${note.name}`
      const button = new MusicButton({
        note: note,
        id: id,
      })
      this._buttons = { ...this._buttons,
        [id]: button
      }
      this._wrapper.appendChild(this._buttons[id].button)
    }
  }
}

export default Piano