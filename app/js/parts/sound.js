class Sound {
  constructor(props) {
    this._props = props
    const ctx = this._props.ctx
    this.currentTime = ctx.currentTime
    this.oscillator = ctx.createOscillator();

    if (this._props.type === 'custom') {
      this.setCustomWave(ctx)
    } else {
      this.oscillator.type = this._props.type || 'sawtooth'
    }

    this.gainNode = ctx.createGain()
  }

  setCustomWave = (ctx) => {
    const steps = 256;
    const imag = new global.Float32Array(steps);
    const real = new global.Float32Array(steps);

    for (let i = 1, j = 1; i < steps; i++) {
      j = j * -1
      imag[i] = 1  / (i * Math.PI)
    }

    const wave = ctx.createPeriodicWave(real, imag);
    this.oscillator.setPeriodicWave(wave)
  }

  connect = (dest) => {
    this.oscillator.connect(this.gainNode)
    this.gainNode.connect(dest)
  }

  play = (value, callBack) => {
    this.oscillator.frequency.setValueAtTime(value, this.currentTime, 0)
    this.gainNode.gain.setValueAtTime(1, this.currentTime, 0)
    this.oscillator.start(this.currentTime);
    this.stop()
  }

  stop = (immidietly) => {
    if (immidietly) {
      this.oscillator.stop(this.currentTime);
    } else {
      this.gainNode.gain.exponentialRampToValueAtTime(0.001, this.currentTime + 1);
      this.oscillator.stop(this.currentTime + 1);
    }
  }
}

export default Sound