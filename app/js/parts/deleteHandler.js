import { createNode } from './helpers'
import { SVGNS, CONTROL_SIZE } from './constants'

class DeleteHandler {
  constructor(props) {
    this._props = { ...props }
    this._rootClassName = 'DeleteHandler'
    this._node = createNode('div', this._rootClassName)
    if (this._props.className) {
      this._node.classList.add(this._props.className)
    }

    this._node.onclick = this._handleClick

    this._iconWrapper = this._renderIconWrapper()
  }

  _handleClick = (e) => {
    if (typeof this.onClick === 'function') {
      this.onClick(e)
    }
  }

  _renderIconWrapper() {
    const node = createNode('svg', `${this._rootClassName}-icon`, true)

    node.setAttribute('viewBox', `0 0 ${CONTROL_SIZE} ${CONTROL_SIZE}`);
    node.setAttribute('version', '1.1');
    node.setAttribute('xmnls', SVGNS);
    node.setAttribute('height', CONTROL_SIZE);
    node.setAttribute('width', CONTROL_SIZE);

    return node
  }

  _renderIcon() {
    const icon = createNode('g', `${this._rootClassName}-path`, true)
    const path1Node = createNode('path', `${this._rootClassName}-pathPart`, true)
    const path1 = `M 1 2 L 2 1 L 17 16 L 16 17 Z`

    path1Node.setAttribute('d', path1)
    const path2Node = path1Node.cloneNode()
    path2Node.setAttribute('transform', `translate(${CONTROL_SIZE}, 0), scale(-1, 1)`)

    icon.appendChild(path1Node)
    icon.appendChild(path2Node)
    return icon
  }

  get node() {
    this._iconWrapper.appendChild(this._renderIcon())
    this._node.appendChild(this._iconWrapper)

    return this._node
  }
}

export default DeleteHandler
