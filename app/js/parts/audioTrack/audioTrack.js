import AudioControl from './parts/audioControl'
import AudioCurve from './parts/audioCurve'
import AudioProgress from './parts/audioProgress'

import DeleteHandler from '../deleteHandler'

class AudioTrack {
  constructor(url, buffers) {
    this._rootClassName = 'AudioTrack'
    this._node = document.createElement('div');
    this._url = url;
    this._buffers = buffers
    this._audio = new Audio(url);
    this._isPlaying = false;

    this.control = new AudioControl({
      isPlaying: this._isPlaying,
      onClick: this.toggle,
      disabled: true,
    })

    this._duration = 0;
    this._currentTime = 0;
    this._currentPosition = 0;
    this._isReady = false
    this._audio.load()
    this._audio.ondurationchange = this.init

    this._isDragging = false

    this._progressComp = new AudioProgress({
      className: `${this._rootClassName}-progress`,
      buffer: this._buffers[0],
    })

    this._progressComp.onChange = this.handleProgressDrag
    this._progressComp.onStopDrag = this.handleProgressStopDrag

    this._height = 0
  }

  init = () => {
    if (!this._audio.duration || this._audio.duration === Infinity || this._audio.duration === NaN) {
      this._audio.currentTime = 24*60*60
      return
    }
    this._duration = this._audio.duration
    this._currentTime = 0
    this._isReady = true
    this.control.setDisabled(false)
    this.setCurrentPosition()
  }

  handleProgressDrag = (val) => {
    this._isDragging = true
    this.setProgress(val)
  }

  handleProgressStopDrag = (val) => {
    this._isDragging = false
    this.setProgress(val)
  }

  setCurrentPosition = () => {
    this._currentPosition = this._currentTime / this._duration * 100
    this.updateTimer(this._currentPosition)
  }

  updateTimer = (pos) => {
    this._progressComp.setValue(pos)
  }

  startWatching = () => {
    if (!this._isReady) {
      return
    }
    this._watchingTimer = setInterval(() => {
      this._currentTime = this._audio.currentTime
      if (!this._isDragging) {
        this.setCurrentPosition()
      }
      if (this._currentTime === this._duration) {
        this._currentTime = 0
        this.setCurrentPosition()
        this.toggle()
        this.stopWatching()
      }
    }, 40)
  }

  setProgress = (percents) => {
    this._currentTime = percents * this._duration / 100
    if (!this._isDragging) {
      this._audio.currentTime = this._currentTime
    }
    this.setCurrentPosition()
  }

  stopWatching = () => {
    clearInterval(this._watchingTimer)
  }

  play = () => {
    this._isPlaying = true;
    this.startWatching()
    this._audio.play();
    this.control.setIsPlaying(false)
  }

  stop = () => {
    this._isPlaying = false;
    this._audio.pause();
    this.stopWatching()
  }

  toggle = () => {
    this._isPlaying ? this.stop() : this.play()
    this.control.setIsPlaying(this._isPlaying)
  }

  onDelete = () => {
    clearTimeout(this._appendTimer)
    this.stop();
    this._node.classList.remove(`${this._rootClassName}--ready`)
    this._node.style.height = 0
    this._appendTimer = setTimeout(() => {
      this._node.remove();
    }, 300)
  }

  renderDeleteButton() {
    const deleteBtn = new DeleteHandler({
      className: `${this._rootClassName}-delete`,
    })
    deleteBtn.onClick = this.onDelete
    return deleteBtn.node
  }

  render = (parent) => {

    const ctrlGroup = document.createElement('div');
    const deleteBtn = this.renderDeleteButton()
    ctrlGroup.className = `${this._rootClassName}-group`

    this.control.render(ctrlGroup)
    this._progressComp.renderTo(ctrlGroup)
    ctrlGroup.appendChild(deleteBtn)

    this._node.appendChild(ctrlGroup)
    this._node.className = this._rootClassName;

    parent.appendChild(this._node);
    const height = this._node.clientHeight
    this._node.style.height = 0

    this._appendTimer = setTimeout(() => {
      this._node.classList.add(`${this._rootClassName}--ready`)
      this._node.style.height = `${height}px`
    }, 40)
  }
}

export default AudioTrack
