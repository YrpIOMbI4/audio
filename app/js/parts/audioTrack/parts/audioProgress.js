import SliderControl from '../../sliderControl'
import AudioCurve from './audioCurve'

import { createNode } from '../../helpers'

const CURVE_WIDTH = 600
const CURVE_HEIGHT = 100

class AudioProgress {

  constructor(props) {
    this._props = props
    this._rootClassName = 'AudioProgress'
    const className = [this._rootClassName]
    if (this._props.className) {
      className.push(this._props.className)
    }
    this._curve = this._renderCurve()
    this.node = new SliderControl({
      ...props,
      min: 0,
      max: 100,
      showCaptions: false,
      handlerNode: createNode('div', `${this._rootClassName}-handler`),
      trackNode: this._curve.node,
      vertical: false,
      highlightProgress: false,
      className: className.join(' '),
    })

    this.node.onChange = this._handleChange
    this.node.onStopDrag = this._handleStopDrag
  }

  _handleChange = (val) => {
    this._curve.highlight(val)
    if (typeof this.onChange === 'function') {
      this.onChange(val)
    }
  }

  _handleStopDrag = (val) => {
    if (typeof this.onStopDrag === 'function') {
      this.onStopDrag(val)
    }
  }

  _renderCurve() {
    const curve = new AudioCurve({
      width: CURVE_WIDTH,
      height: CURVE_HEIGHT,
      buffer: this._props.buffer,
      className: `${this._rootClassName}-curve`
    })
    return curve
  }

  setValue = (pos) => {
    this.node.setValue(pos)
    this._curve.highlight(pos)
  }

  renderTo = (parent) => {
    this.node.render(parent)
  }
}

export default AudioProgress
