import { createNode } from '../../helpers'

class AudioCurve {
  constructor(props) {
    this._props = props
    this._rootClassName = 'AudioCurve'

    this._rects = {}
    this._svg = this.renderSvg()
    this._path = this.renderCurve()
  }

  renderSvg() {
    const className = this._props.className ? `${this._rootClassName} ${this._props.className}` : this._rootClassName
    const node = createNode('svg', className, true, this._props.width, this._props.height)
    return node
  }

  renderRect(x, y, width, height) {
    const node = createNode('rect', `${this._rootClassName}-rect`, true)
    node.setAttribute('x', x)
    node.setAttribute('y', y)
    node.setAttribute('width', width)
    node.setAttribute('height', height)

    return node
  }

  highlight = (percents) => {
    const lastRect = Math.ceil(this._props.width * percents / 100)

    let i = 0
    for (const rect in this._rects) {
      if (i <= lastRect) {
        this._rects[i].classList.add(`${this._rootClassName}-rect--active`)
      } else {
        this._rects[i].classList.remove(`${this._rootClassName}-rect--active`)
      }
      i++
    }
  }

  renderCurve() {
    const node = createNode('g', `${this._rootClassName}-path`, true)

    const width = this._props.width
    const height = this._props.height
    const buffer = this._props.buffer

    const step = Math.ceil( buffer.length / width );

    const amp = height / 2;

    for (let i = 0; i < width; i++) {
      let min = 1;
      let max = -1;
      for (let j = 0; j < step; j++) {
        const datum = buffer[(i * step) + j];
        min = datum < min ? datum : min;
        max = datum > max ? datum : max;
      }
      const x = i
      const y = (1 + min) * amp
      const rectWidth = 1
      const rectHeight = Math.max(1, (max - min) * amp)
      this._rects[i] = this.renderRect(x, y , rectWidth, rectHeight)
      node.appendChild(this._rects[i])
    }
    return node

  }

  renderTo(parent) {
    if (!parent) {
      return
    }
    this._svg.appendChild(this._path)
    parent.appendChild(this._svg)
  }

  get node() {
    this._svg.appendChild(this._path)
    return this._svg
  }
}

export default AudioCurve
