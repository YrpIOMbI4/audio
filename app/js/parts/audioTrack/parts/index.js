import AudioControl from './audioControl'
import AudioCurve from './audioCurve'
import AudioProgress from './audioProgress'

export default {
  AudioControl,
  AudioCurve,
  AudioProgress,
}
