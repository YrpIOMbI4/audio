import { CONTROL_SIZE } from '../../constants'
import { createNode } from '../../helpers'

const AUDIO_CONTROL_WIDTH = 15;
const AUDIO_CONTROL_HEIGHT = CONTROL_SIZE;

class AudioControl {
  constructor(props) {
    this._rootClassName = 'playButton'
    this.props = props

    this.node = createNode('div', this._rootClassName)
    this.node.onclick = this.handleClick

    this.svgPath = this._renderIconPath()
    this.svg = this._renderIcon()

    this._disabled = false
    this.setDisabled(this.props.disabled)

    this._init()
  }

  handleClick = (e) => {
    if (!this._disabled) {
      this.props.onClick(e)
    }
  }

  _init() {
    this.setState(this._isPlaying)
    this.svgPath.setAttribute('class', `${this._rootClassName}__iconPath`)
  }

  _renderIcon() {
    const svg = createNode('svg', `${this._rootClassName}__icon`, true, AUDIO_CONTROL_WIDTH, AUDIO_CONTROL_HEIGHT)
    return svg
  }

  _renderIconPath() {
    const path = createNode('path', `${this._rootClassName}__iconPath`, true)
    return path
  }

  setIsPlaying = (isPlaying) => {
    this._isPlaying = isPlaying
    this.setState(isPlaying)
  }

  setState = (isPlaying) => {
    const h = AUDIO_CONTROL_HEIGHT
    const w = AUDIO_CONTROL_WIDTH
    const pauseW = w / 3
    const playPath = `M 0 ${h} L 0 0 L ${w / 2} ${h / 4} L ${w / 2} ${h - h / 4} Z M ${w} ${h / 2} L ${w} ${h / 2} L ${w / 2} ${h / 4} L ${w / 2} ${h - h / 4} Z`
    const pausePath = `M 0 ${h} L 0 0 L ${pauseW} 0 L ${pauseW} ${h} Z M ${w} ${h} L ${w} 0 L ${w - pauseW} 0 L ${w - pauseW} ${h} Z`
    if (isPlaying) {
      this.svgPath.setAttribute('d', pausePath)
    } else {
      this.svgPath.setAttribute('d', playPath)
    }
  }

  setDisabled = (disabled) => {
    this._disabled = disabled
    if (disabled) {
      this.node.classList.add(`${this._rootClassName}--disabled`)
    } else {
      this.node.classList.remove(`${this._rootClassName}--disabled`)
    }
  }

  render = (parent) => {
    this.svg.appendChild(this.svgPath)
    this.node.appendChild(this.svg)
    parent.appendChild(this.node)
  }
}

export default AudioControl
