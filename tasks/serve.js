'use-strict'

const browserSync = require('browser-sync')
const gulp = require('gulp');
const config = require('../config/devServer');

module.exports = function (options) {
  const config = {
    server: {
      baseDir: `./${options.dest}`,
    },
    tunnel: true,
    host: 'localhost',
    port: 3000,
    logPrefix: 'Yo-yo!'
  }
  return function () {
    browserSync(config);
    browserSync.watch(`${options.dest}/**/*.*`).on('change', browserSync.reload).on('error', function (error) {
      console.log(error.message)
    })
  }
}