'use-strict'

const $ = require('gulp-load-plugins')();
const gulp = require('gulp');
const combine = require('stream-combiner2').obj;

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV === 'development'

module.exports = function (options) {
  return function () {
    return combine(
      gulp.src(options.src),
      $.if(isDev, $.sourcemaps.init()),
      $.sass(),
      $.minifyCss(''),
      $.concat('style.min.css'),
      $.autoprefixer({
        browsers: ['last 10 versions'],
        cascade: false
      }),
      $.csscomb(),
      $.if(isDev, $.sourcemaps.write()),
      gulp.dest(options.dest)
    ).on('error', $.notify.onError((err) => {
      return {title: 'Styles error', message: err.message }
    }))
  }
}