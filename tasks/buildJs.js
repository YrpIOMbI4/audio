'use-strict'

const $ = require('gulp-load-plugins')();
const browserify = require('gulp-browserify');
const babelify = require('babelify');
const gulp = require('gulp');
const combine = require('stream-combiner2').obj;

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV === 'development'

module.exports = function (options) {
  return function () {
    return combine(
      gulp.src(options.src),
      $.if(isDev, $.sourcemaps.init()),
      $.browserify({ insertGlobals : true }),
      $.uglify(),
      $.if(isDev, $.sourcemaps.write()),
      gulp.dest(options.dest)
    ).on('error', $.notify.onError((err) => {
      return {
        title: 'JS error',
        message: err.message,
      }
    }))
  }
}
