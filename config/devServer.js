'use-strict'
const paths = require('./paths');

module.exports = {
  server: {
    baseDir: `./${paths.build.root}`,
  },
  tunnel: true,
  host: 'localhost',
  port: 3000,
  logPrefix: 'Yo-yo!'
};