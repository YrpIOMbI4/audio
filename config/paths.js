'use-strict'

const BUILD_DIR = 'build'
const APP_DIR = 'app'
const TMP_DIR = 'tmp'

module.exports = {
  build: {
    root: BUILD_DIR,
    html: `${BUILD_DIR}/`,
    js: `${BUILD_DIR}/js`,
    css: `${BUILD_DIR}/css`,
    img: `${BUILD_DIR}/img`,
    fonts: `${BUILD_DIR}//fonts`,
  },
  src: {
    root: APP_DIR,
    html: `${APP_DIR}/*.html`,
    js: `${APP_DIR}/js/main.js`,
    style: [`${APP_DIR}/style/main.scss`, `${APP_DIR}/style/components/*.scss`],
    img: `${APP_DIR}/img/**/*.+(png|jpg|jpeg|gif|svg)`,
    fonts: `${APP_DIR}/fonts/**/*.*`,
  },
  watch: {
    html: `${APP_DIR}/**/*.html`,
    js: `${APP_DIR}/js/**/*.js`,
    style: `${APP_DIR}/style/**/*.scss`,
    img: `${APP_DIR}/img/**/*.+(png|jpg|jpeg|gif|svg)`,
    fonts: `${APP_DIR}/fonts/**/*.*`,
  },
  clean: `./${BUILD_DIR}`,
}